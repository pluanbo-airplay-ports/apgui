#pragma once
#include "SuiGen.h"
#include "QuApExtensions.h"
#include <map>


struct ApSoundObject
{
	std::string mFilename;
	bool mLoaded;
	ApSoundObject():mLoaded(false){}
	ApSoundObject(std::string aFilename);
	bool operator!=(ApSoundObject & o)
	{ return o.mFilename != mFilename; }
};

class ApSoundInterface : public Gui::SoundInterface<ApSoundObject>
{
	std::map<int,ApSoundObject > mChannelMap;
public:
	void DeleteSound(ApSoundObject snd);
	ApSoundObject LoadSound(std::string sFile);
	void PlaySound(ApSoundObject snd, int nChannel = -1, bool bLoop = false);
	void StopSound(int channel = -1);
};
