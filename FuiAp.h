#pragma once
#include "General.h"
#include <string>
#include <sstream>
#include <iostream>
#include "s3eFile.h"

using namespace Gui;

class ApInStream2: public InStreamHandler
{
	std::string sData;
	s3eFile * mFile;
public:
	ApInStream2(std::string sFileName, bool bBinary = false):InStreamHandler(0) 
	{
		//TODO look for file in the user data section first.
		//or perhaps a little safer is to look for in regular place, if not found, then look in user data section :)
		mFile = s3eFileOpen(sFileName.c_str(),"r");
		if(mFile == NULL)
			throw SimpleException(sFileName + " failed to read :( in ApInStream");
		if(!bBinary)
		{
			const int buffSize = 255;
			char buffer[buffSize+1];
			while(s3eFileReadString(buffer,buffSize,mFile) != NULL)
				sData.append(buffer);
		}
		else
		{
			const int buffSize = 256;
			char buffer[buffSize];
			bool flag = false;
			while(!flag)
			{
				uint32 bytesRead = s3eFileRead(buffer,sizeof(char),buffSize,mFile);
				//we are at the end
				if(bytesRead < buffSize*sizeof(char))
					flag = true;
				sData.append(buffer,bytesRead/sizeof(char));
			}

		}
		//if(s3eFileGetError() != S3E_FILE_ERR_EOF)
		//	throw SimpleException(sFileName + " failed to read :( in ApInStream");
		s3eFileClose(mFile);
		if(!bBinary)
			pStr = new std::istringstream(sData);
		else
			pStr = new std::istringstream(sData, std::ios_base::in | std::ios_base::binary);

		std::cout << sFileName << " read succesfully" << std::endl;
	}
	~ApInStream2()
	{
	}
};

class ApOutStream2: public OutStreamHandler
{
	std::string sData;
	std::string sFileName;
	s3eFile * mFile;
	bool mBinary;
	std::ostringstream* pStrStr;
public:
	ApOutStream2(std::string sFileName_, bool aBinary = false):OutStreamHandler(0), sFileName(sFileName_), mBinary(aBinary)
	{
		if(mBinary)
			pStrStr = new std::ostringstream(sData,std::ios_base::out | std::ios_base::binary);
		else
			pStrStr = new std::ostringstream(sData);
		pStr = pStrStr;
		mFile = s3eFileOpen(sFileName.c_str(),"w");
		if(mFile == NULL)
			throw SimpleException(sFileName + " failed to open :( in ApOutStream");

	}
	~ApOutStream2()
	{
		//TODO test me

		std::string sOut = pStrStr->str();

		//if(mBinary)
			s3eFileWrite(sOut.c_str(),sizeof(char),sOut.length(),mFile);
		//else
		//	s3eFilePrintf(mFile,sOut.c_str());
		s3eFileClose(mFile);
	}
};


class ApFileManager2 : public FileManager
{
public:
	/*virtual*/ SP<OutStreamHandler> WriteFile(std::string s)
	{
		return new ApOutStream2(s);
	}
	/*virtual*/ SP<InStreamHandler> ReadFile(std::string s)
	{
		return new ApInStream2(s);
	}
};

