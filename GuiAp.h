#include "GuiGen.h"
#include "QuImage.h"
#include "QuGlobals.h"
#include "QuCamera.h"
#include "QuInterface.h"
#include "QuUtils.h"
#include "SuiGen.h"
#include "FuiAp.h"

#ifdef BATCH
	typedef QuOrthoCamera CameraType;
#else
	typedef QuViewportBasicCamera CameraType;
#endif

using namespace Gui;
class ApImage : public Image
{
	QuStupidPointer<QuDrawObject> mDraw;
	QuStupidPointer<QuDynamicImage> mImage;
public:
	ApImage(Size _sz):Image(_sz)
	{
		int32 before = s3eMemoryGetInt(S3E_MEMORY_FREE);

		mDraw = QuStupidPointer<QuDrawObject>(new QuDrawObject());
		mDraw->setCount(4);
		float * rectCoords = new float[4*3];
		memcpy(rectCoords,GUI_RECT_COORDS,sizeof(float) * 4 * 3);
		mDraw->loadVertices(rectCoords);
		mImage = QuStupidPointer<QuDynamicImage>(new QuDynamicImage(_sz.x,_sz.y));
		mDraw->loadImage(mImage.safeCast<QuBaseImage>());

		int32 after = s3eMemoryGetInt(S3E_MEMORY_FREE);
		//std::cout << "used: " << before-after << std::endl;
		//std::cout << "remaining: " << s3eMemoryGetInt(S3E_MEMORY_FREE) << std::endl;
	}
	~ApImage()
	{
		mDraw.setNull();
		mImage.setNull();
	}
	void SetPixel(Point p, const Color & c)
	{
		mImage->setPixel(p.x,p.y,c.R,c.G,c.B,c.nTransparent);
	}
	Color GetPixel(Point p) const
	{
		unsigned char r,g,b,a;
		mImage->getPixel(p.x,p.y,r,g,b,a);
		return Color(r,g,b,a);
	}
	QuStupidPointer<QuDynamicImage> getImage()
	{
		return mImage;
	}
	QuStupidPointer<QuDrawObject> getDraw()
	{
		return mDraw;
	}
};

struct ApGuiBatchDrawObject
{
	int o;
	Rectangle r;
	Color c;
	Point p;

	QuStupidPointer<QuDynamicImage> img;
	QuStupidPointer<QuDrawObject> draw;

	ApGuiBatchDrawObject(int ao, Rectangle ar, Color ac):o(ao),r(ar),c(ac){}
	ApGuiBatchDrawObject(int ao, Rectangle ar, Point ap, ApImage * aimg):o(ao),p(ap),r(ar),img(aimg->getImage()),draw(aimg->getDraw()){}
};
class ApGraphicalInterface : public GraphicalInterface<ApImage *>
{
	CameraType camera;
	std::map<ApImage *, std::list<ApGuiBatchDrawObject> > mBatch;
	int mDrawCounter;
public:
	ApGraphicalInterface(Size _sz):camera(_sz.y,_sz.x,_sz.x),mDrawCounter(0)
	{
		if(G_GAME_GLOBAL.isLandscapeOriented())
		{
			camera = CameraType(_sz.x,_sz.y,_sz.y);
			camera.setRotation(G_DR_0);
		}
		else
			camera.setRotation(G_DR_90);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDisable(GL_LIGHTING);
#ifdef BATCH
		glDepthMask(true);
		glEnable(GL_DEPTH_TEST);
		//could be greater than or equal to and 0
		glAlphaFunc ( GL_GREATER, 0.1 ); 
		glEnable ( GL_ALPHA_TEST );
#else
		glDepthMask(false);
		glDisable(GL_DEPTH_TEST);
#endif
		
	}
	//not gui relevant
	GDeviceRotation getCurrentCameraRotation()
	{
		return camera.getRotation();
	}
	QuScreenCoord convertScreenToCameraCoord(QuScreenCoord & aCrd)
	{
		return camera.convertScreenToCameraCoord(aCrd);
	}
	ApImage * LoadImage(std::string sFileName)
	{
		SP<ApInStream2> streem = new ApInStream2(sFileName,true);
		std::istream& ifs = streem->GetStream();
        if(ifs.fail())
            throw GraphicalInterfaceSimpleException("GraphicalInterface", "LoadImage",
            "Cannot load image (cannot open file " + sFileName + ")");
        
        try {return GuiLoadImage(&ifs, this);}
        catch (SimpleException& se)
        {
            GraphicalInterfaceSimpleException ge("GraphicalInterface", "LoadImage",
            "Cannot load image from file " + sFileName);
            ge.InheritException(se);
            throw ge;
        }
	}
    void SaveImage(std::string sFileName, ApImage * pImg)
	{
		PointerAssert<NullPointerGIException>("GraphicalInterface", "SaveImage", "pImg", pImg);

        SP<ApOutStream2> streem = new ApOutStream2(sFileName,true);
		std::ostream& ofs = streem->GetStream();
        
        if(ofs.fail())
            throw GraphicalInterfaceSimpleException("GraphicalInterface", "SaveImage",
            "Cannot save image (cannot open file " + sFileName + ")");
        
        try{GuiSaveImage(&ofs, GetImage(pImg));}
        catch (SimpleException& se)
        {
            GraphicalInterfaceSimpleException ge("GraphicalInterface", "SaveImage",
            "Cannot save image into file " + sFileName);
            ge.InheritException(se);
            throw ge;
        }
	}
	ApImage * GetBlankImage(Size s) 
	{ 
		return new ApImage(s); 
	}

	
	

#ifdef BATCH
	void DrawImage(Point _p, ApImage * _i, Rectangle _r, bool refresh = true)
	{
		mBatch[_i].push_back(ApGuiBatchDrawObject(mDrawCounter,_r,_p,_i));
		mDrawCounter++;
	}
	void DrawRectangle(Rectangle _r, Color _c, bool refresh = true)
	{
		mBatch[NULL].push_back(ApGuiBatchDrawObject(mDrawCounter,_r,_c));
		mDrawCounter++;
	}
	void draw()
	{
		if(mBatch.size() == 0)
			return;
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glMatrixMode(GL_TEXTURE);
		glLoadIdentity();
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();

		camera.setRotation(G_DEVICE_ROTATION,G_RESTRICT_LANDSCAPE);
		camera.setScene();

		for(  std::map<ApImage *, std::list<ApGuiBatchDrawObject> >::iterator it = mBatch.begin(); it != mBatch.end(); it++)
		{
			bool flag = true;
			QuStupidPointer<QuDynamicImage> img = NULL;
			QuStupidPointer<QuDrawObject> draw = NULL;
			for(  std::list<ApGuiBatchDrawObject>::iterator jt = it->second.begin(); jt != it->second.end(); jt++)
			{
				if(it->first == NULL)
					DrawRectangle(jt->r,jt->c,jt->o,false);
				else
				{
					glPushMatrix();
					//set the transform
					Point _p = jt->p;
					Rectangle _r = jt->r;
					float xTrans = _p.x - camera.getRotatedWidth()/2.0f + _r.sz.x/2.0f;
					float yTrans = -_p.y + camera.getRotatedHeight()/2.0f - _r.sz.y/2.0f;
					glTranslatef(xTrans,yTrans,3*jt->o);
					glScalef(_r.sz.x,_r.sz.y,1);
		
					img = jt->img;
					draw = jt->draw;

					//set the UV coords
					float px = quClamp<float>(_r.p.x/(float)img->getPotW(),0,1);
					float py = quClamp<float>(_r.p.y/(float)img->getPotH(),0,1);
					float pw = quClamp<float>(px + _r.sz.x/(float)img->getPotW(),0,1);
					float ph = quClamp<float>(py + _r.sz.y/(float)img->getPotH(),0,1);
					float uv[8] = {px,py,pw,py,px,ph,pw,ph};
					draw->loadTexCoords<8>(ARRAYN<float,8>(uv));

					if(flag)
					{
						draw->enable();
						flag = false;
					}
					
					draw->enableTexture();
					draw->draw();
					glPopMatrix();
				}
			}
			if(!draw.isNull())
				draw->disable();
		}
		glPopMatrix();
		mDrawCounter = 0;
		mBatch.clear();
	};
#else
	void DrawImage(Point _p, ApImage * _i, Rectangle _r, bool refresh = true)
	{
		glMatrixMode(GL_TEXTURE);
		glLoadIdentity();


		//set the camera position
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		camera.setRotation(G_DEVICE_ROTATION,G_RESTRICT_LANDSCAPE);
		camera.setScene();

		//set the transform
		float xTrans = _p.x - camera.getRotatedWidth()/2.0f + _r.sz.x/2.0f;
		float yTrans = -_p.y + camera.getRotatedHeight()/2.0f - _r.sz.y/2.0f;
		glTranslatef(xTrans,yTrans,0);
		glScalef(_r.sz.x,_r.sz.y,1);
		
		QuStupidPointer<QuDynamicImage> img = _i->getImage();
		QuStupidPointer<QuDrawObject> draw = _i->getDraw();

		//set the UV coords
		float px = quClamp<float>(_r.p.x/(float)img->getPotW(),0,1);
		float py = quClamp<float>(_r.p.y/(float)img->getPotH(),0,1);
		float pw = quClamp<float>(px + _r.sz.x/(float)img->getPotW(),0,1);
		float ph = quClamp<float>(py + _r.sz.y/(float)img->getPotH(),0,1);
		// for non percetange value coordinates
		/*
		float px = _r.p.x;
		float py = _r.p.y;
		float pw = _r.sz.x;
		float ph = _r.sz.y;*/

		float uv[8] = {px,py,pw,py,px,ph,pw,ph};
		draw->loadTexCoords<8>(ARRAYN<float,8>(uv));

		//draw it
		draw->autoDraw();
		glPopMatrix();

		if(refresh)
			RefreshAll();
	}
	void DrawRectangle(Rectangle _r, Color _c, bool refresh = true)
	{
		DrawRectangle(_r,_c,0,refresh);
	}
	//dummy function
	void draw(){}
#endif
	
	void DrawRectangle(Rectangle _r, Color _c, int order, bool refresh = false)
	{
		float coords[4][3] = 
		{
			_r.p.x,			_r.p.y, 				0,
			_r.p.x + _r.sz.x, 	_r.p.y,				0,
			_r.p.x,			_r.p.y + _r.sz.y,		0,
			_r.p.x + _r.sz.x,	_r.p.y + _r.sz.y,	0
		};
		float colors[4][4] = 
		{
			_c.R/255.0f,		_c.G/255.0f,	_c.B/255.0f,	_c.nTransparent/255.0f,
			_c.R/255.0f,		_c.G/255.0f,	_c.B/255.0f,	_c.nTransparent/255.0f,
			_c.R/255.0f,		_c.G/255.0f,	_c.B/255.0f,	_c.nTransparent/255.0f,
			_c.R/255.0f,		_c.G/255.0f,	_c.B/255.0f,	_c.nTransparent/255.0f
		};
		
		//TODO test
		QuDrawObject d;
		d.setCount(4);
		d.loadVertices(ARRAYN<float,12>::conv2<4,3>(coords));
		d.loadColors(ARRAYN<float,16>::conv2<4,4>(colors));
		
		//TODO
		//glRotatef(90,0,0,1);
		//should be G_DEVICE_ROTATION rotade left by 90 degrees
		camera.setRotation(G_DEVICE_ROTATION,G_RESTRICT_LANDSCAPE);
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		camera.setScene();
		int w = camera.getRotatedWidth();
		int h = camera.getRotatedHeight();
		glScalef(1,-1,1);
		glTranslatef( -w/2.0f, -h/2.0f, 3 * order);
		d.autoDraw();
		glPopMatrix();

		if(refresh)
			RefreshAll();
			
	}
	void DeleteImage(ApImage * _img){ delete _img; }
	Image * GetImage(ApImage * _img) const { return _img; }
	void Refresh(Rectangle _r)
	{
	}
	void RefreshAll()
	{
//#ifndef BATCH
#ifdef ENABLE_REFRESH
		draw(); //this is a stupid ass hack for dragongame loading screen
		IwGLSwapBuffers();
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
#endif
//#endif
	}
};



