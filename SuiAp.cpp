#include "SuiAp.h"
#include "QuApExtensions.h"
#include "QuGlobals.h"
#include "QuSoundManager.h"
#define STOP_ALL //TODO move me somelpace else
std::string getFileNameFromPath(std::string aFilename)
{
	std::string path = aFilename;
	std::string filename;
	size_t pos = path.find_last_of("\\");
	if(pos != std::string::npos)
		filename.assign(path.begin() + pos + 1, path.end());
	else
		filename = path;
	return filename;
}

std::string getFilePath(std::string aFilename)
{
	return aFilename.substr(0,aFilename.length() - getFileNameFromPath(aFilename).length());
}

int countUnderscores(std::string aFilename)
{
	int underscores = 0;
	for(int i = 0; i < aFilename.length(); i++)
	{
		if(aFilename[i] == '_')
			underscores++;
		else break;
	}
	return underscores;
}

ApSoundObject::ApSoundObject(std::string aFilename):mLoaded(false),mFilename(aFilename)
{
	
	std::string fn = getFileNameFromPath(aFilename);
	std::string pt = getFilePath(aFilename);
	int underscores = countUnderscores(fn);
	fn = fn.substr(underscores,fn.length() - underscores);
	G_SOUND_MANAGER.loadSound(pt+fn);
	mLoaded = true;
}
void ApSoundInterface::DeleteSound(ApSoundObject snd)
{
	//sound manager actually gets destroyed before this gets called due to globals issue, we will just this get cleaned up by the program quitting
	if(&G_GAME_GLOBAL.getSoundManager() != NULL)
		G_SOUND_MANAGER.safeDeleteSound(snd.mFilename);
	snd.mLoaded = false;
}
ApSoundObject ApSoundInterface::LoadSound(std::string sFile)
{
	return ApSoundObject(sFile);
	//return ApSoundObject();
}
void ApSoundInterface::PlaySound(ApSoundObject snd, int aChannel, bool bLoop)
{
#ifdef _DEBUG
	std::cout << "playing sound " << snd.mFilename << " looping " << bLoop << std::endl;
#endif

	std::string fn = getFileNameFromPath(snd.mFilename);
	std::string pt = getFilePath(snd.mFilename);
	int underscores = countUnderscores(fn);
	fn = fn.substr(underscores,fn.length() - underscores);

	QuStupidPointer<QuSoundStructInterface> sp = G_SOUND_MANAGER.getSound(pt+fn);
	QuApRawSoundStruct * ptr = dynamic_cast<QuApRawSoundStruct *>(sp.rawPointer());
	if(ptr) ptr->setRate(22050/(underscores+1));
	sp->setLoop(bLoop);
	/* broken because this will stop channels on sounds that are already finished playing and totally screw thigns up since aChannel does not sync with the system channels
	if(mChannelMap.find(aChannel) != mChannelMap.end())
		if(snd != mChannelMap[aChannel])
			StopSound(aChannel);*/
	sp->play();
	mChannelMap[aChannel] = snd;
}
void ApSoundInterface::StopSound(int aChannel)
{
#ifdef STOP_ALL
	s3eSoundStopAllChannels();
	return;
#endif

	if(mChannelMap.find(aChannel) == mChannelMap.end())
		return;
#ifdef _DEBUG
	std::cout << "stopping sound " << mChannelMap[aChannel].mFilename << std::endl;
#endif

	std::string fn = getFileNameFromPath(mChannelMap[aChannel].mFilename);
	std::string pt = getFilePath(mChannelMap[aChannel].mFilename);
	int underscores = countUnderscores(fn);
	fn = fn.substr(underscores,fn.length() - underscores);

	std::cout << G_SOUND_MANAGER.getSound(pt+fn)->isPlaying() << std::endl;
	G_SOUND_MANAGER.getSound(pt+fn)->stop();
	std::cout << G_SOUND_MANAGER.getSound(pt+fn)->isPlaying() << std::endl;
}